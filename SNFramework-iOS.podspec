Pod::Spec.new do |s|
  s.platform = :ios
  s.ios.deployment_target = '9.0'
  s.name = 'SNFramework-iOS'
  s.version = '0.78.0'
  s.author = { "Bradley Wells" => "bradley@sensornetworks.co.za" }
  s.license = 'Proprietary'
  s.homepage = 'http://:www.sensornetworks.co.za'
  s.summary = 'Sensor Networks iOS library to use the Sensor Networks IoT Platform.'
  
  s.source = { 
    :http => 'https://bitbucket.org/SensorNetworksTeam/snframework-ios/raw/e6666d3d322f9c5e9b6296508f43660687b19ad6/SNFramework.framework.zip'
  }
  s.vendored_frameworks = 'SNFramework.framework'
  s.framework = 'UIKit'
  s.dependency 'Starscream'
  s.dependency 'SwiftyJSON'
  s.dependency 'Alamofire', '~> 4.2.0'
end
