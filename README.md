# SNFramework

The Sensor Networks iOS framework interacting with the Sensor Networks Platform.

## Getting Started

These instructions will get this framework setup into your iOS XCode project. At the moment it is distributed via the Git repo only but in the future we will provide a Cocoapods private repo. This will allow you to install via the Cocoapods dependency manager.


### Prerequisites

There are third party libraries that are needed for use with SNFramework. Although you can install them manually, we suggest the best way to install it is via Cocoapods.
If you do not know how to install Cocoapods, you can find out how here [Cocoapods](https://cocoapods.org/).

### Installing

In order to use the SNFramework, you will need to add it to your project. The easy way is to drag and drop it onto your XCode project. Make sure you copy the snframework.framework file into your project. 

An important step is to open the Target target build settings, scroll down to the Embedded Binaries section and make sure the snframework.framework is listed there.

Now we will need to setup the rest of the 3rd party libraries. The Podfile is provided so that Cocoapods can use it to setup the rest of the 3rd partyy libraries.
If you do not have a podfile of your own, you will need to edit the Podfile provided. Edit the Podile and add you project target name and replace the "<Target>" text.

Then open the Mac Terminal and run the following command in the project root directory.

```
pod install
```

This will install all 3rd party libraries needed by the the application.

Once this is finished you should open the XCode workspace.

### Using the framework
In order to use SNFramework in any file, you first have to import the Module:

```
import SNFramework
```

The SNFramework uses websockets to communicate with the Sensor Networks Platform. The SNFramework follows an iOS delegate architecture which passes messages of the framework to a SNFrameworkDelegate. Usually this is the UIApplicationDelegate but it could be any controller you choose. The following methods are passed back to the SNFrameworkDelegate:

```
- didConnect
- didDisconnect
- didLogin
- didLogOut
```

You intitlaize the SNFramework as follows:

```
snframework = SNFramework.sharedInstance
snframework?.setDelegate(delegate: self)
snframework?.connect()
```
The SNFramework also uses the Observer pattern for dealing with devices and listening for callbacks. When needed the controller will listen for callback by adding an observer as follows:

```
NotificationCenter.default.addObserver(self, selector: #selector(gotGeysers) , name: Notification.Name(SNFramework.NOTIFICATION_GEYSERS_LOADED) , object: nil)
```

The framework provides a list of notifications names as static variables  on the SNFramework class.

## Authentication
The platform utilizes OAuth to authenticate, so we have limited the authentication of the server to third party providers. Once you are setup as a provider for the Sensor Networks Platform, you will be able to login into the SNFramework by passing a Accesstoken. This Accesstoken will be provided via your own Authentication system and used by our Sensor Networks Platform to validate against your Authentication system. To login once the framework has connected, you do the following, usually in the didConnect callback method: 

```
func didConnect() {
	snframework = SNFramework.sharedInstance
	snframework.login(authToken: token)
}
```
The framework will authenticate and callback into the didLogin method.

## Fetching device data 
Once you have logged in you can fetch a list of devices that is available to you via the Sensor Networks Platform. At the moment we have 2 IoT devices available, the SNGeyser & the SNTracker. This list will exand in the future as our list of devices grows. Fetching the list of geysers is as easy as the following:

```
do {
     try snframework?.getGeysers()
   } catch let error as SNFrameworkError {
    	// handle error graciously here
   }catch{
    // handle error graciously here
}
```

Once this is done, you will get a callback on the SNFramework.NOTIFICATION_GEYSERS_LOADED notification. TO get the Array of SNGeysers, you will do the following:

```
let list = snframework.geysers.allValues()
let geyser = list[0] as SNGeyser
```

## Built With
* [Cocoapods](https://cocoapods.org/) - CocoaPods is a dependency manager for Swift and Objective-C Cocoa projects
* [Starscream](https://github.com/daltoniam/Starscream) - Websockets in swift for iOS and OSX
* [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON) - The better way to deal with JSON data in Swift
* [Alamofire](https://github.com/Alamofire/Alamofire) - Elegant HTTP Networking in Swift

There are other libraries used within SNFramework which can be seen on the SNFramework link above.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/SensorNetworksTeam/snframework-ios). 

## Authors

* **Bradley Wells** * bradley@sensornetworks.com

## License

This project is licensed under the Propriety license on behalf of Nedbank - see the [LICENSE.md](LICENSE.md) file for details
